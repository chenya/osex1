#include "prog1_run.h"

/*
int getNumberOfPairsFromUser(void)
{
    int number_of_pairs;
    printf("Enter Number of pairs numbers: ");
    scanf("%d",&number_of_pairs);
    return number_of_pairs;
}

void getDateAndSecondsPairFromUser(int* days_since_year_2000, int* number_of_seconds_this_day)
{
    scanf("%d %d",days_since_year_2000, number_of_seconds_this_day);
}

void fillDaysInEachMonth(int* days_in_month )
{
    days_in_month[JANUARY]=JANUARY_DAYS;
    days_in_month[FEBRUARY]=FEBRUARY_DAYS;
    days_in_month[MARCH]=MARCH_DAYS;
    days_in_month[APRIL]=APRIL_DAYS;
    days_in_month[MAY]=MAY_DAYS;
    days_in_month[JUNE]=JUNE_DAYS;
    days_in_month[JULY]=JULY_DAYS;
    days_in_month[AUGUST]=AUGUST_DAYS;
    days_in_month[SEPTEMBER]=SEPTEMBER_DAYS;
    days_in_month[OCTOBER]=OCTOBER_DAYS;
    days_in_month[NOVEMBER]=NOVEMBER_DAYS;
    days_in_month[DECEMBER]=DECEMBER_DAYS;
}

int extractMonthAndDays(int date_in_days, int* days_in_month, int* extract_month)
{
    int days_in_current_year;
    int curr_month;
    int days_delta;

    days_in_current_year=date_in_days % DAYS_IN_YEAR ;
    days_delta = days_in_current_year;

    for (curr_month = JANUARY; (curr_month < MONTHS_IN_YEAR) && ((days_delta - days_in_month[curr_month])   >= 0); curr_month++) {
        days_delta -= days_in_month[curr_month];
    }
 
    *extract_month = curr_month+1;
    return (days_delta==0) ? 1 : (days_delta+1);
}

void transformDateInDaysToRealDate(int date_in_days, int* days_in_month, int* day, int* month, int* year)
{
    *year=EXTRACT_YEAR(date_in_days);
    *day=extractMonthAndDays(date_in_days, days_in_month, month);
}

void getDayMonthYear(int date_in_days, int* day, int* month, int* year)
{
    int days_in_month[MONTHS_IN_YEAR];
    fillDaysInEachMonth(days_in_month);
    
    transformDateInDaysToRealDate(date_in_days, days_in_month, day, month, year);
}

void printActualDate(int date_in_days)
{
    int day, month, year;
    getDayMonthYear(date_in_days, &day, &month, &year);

    printf("%d/%d/%d",day,month,year);
}

void getHourtMinuteSecond(int seconds, int* hour, int* minute, int* second)
{
    *hour=*minute=*second=0;
    
    if (seconds != 0) {
        *hour+=EXTRACT_HOUR(seconds); 
        *minute+=(seconds - (*hour)*SECONDS_IN_HOUR ) / SECONDS_IN_MINUTE; 
        *second+=(seconds - (*hour)*SECONDS_IN_HOUR ) % SECONDS_IN_MINUTE; 
    }
}

void printActualTime(int seconds)
{
    int hour;
    int minute;
    int second;
    
    getHourtMinuteSecond(seconds, &hour, &minute, &second);
    //printf("Actual time: %.2d:%.2d:%.2d",hour,minute,second);
    printf("%.2d:%.2d:%.2d",hour,minute,second);
}

void printActualDateAndTime(int date, int seconds)
{
    printActualDate(date); 
    printf(" ");
    printActualTime(seconds); 
    printf("\n");

}

void getPairsOfDateAndTimeFromUser(struct DT_int* DT_int_Arr, int num_of_pairs)
{
    int i;

    for (i = 0; i < num_of_pairs; i++) {
        getDateAndSecondsPairFromUser(&(DT_int_Arr[i].date), &(DT_int_Arr[i].seconds));
    }
}

void printPairsOfDateAndTime(struct DT_int* DT_int_Arr, int num_of_pairs)
{
    int i;
    for (i = 0; i < num_of_pairs; i++) {
        printActualDateAndTime(DT_int_Arr[i].date, DT_int_Arr[i].seconds) ;
    }
    
}



void writeDatesToFile(struct DT_int* DT_int_arr, int num_of_dates)
{
    FILE *DT_file=openFile(DT_INT_FILE, BINARY_WRITE);
    int i;
    
    for (i = 0; i < num_of_dates; i++) {
        writeDT_intStructToBinFile(&(DT_int_arr[i]), DT_file);
    }

    closeFile( DT_file);

}
*/

void run_prog1(void)
{
    int n;
    
    struct DT_int DT_int_Arr[MAX_DT];

    n=getNumberOfPairsFromUser();
    getPairsOfDateAndTimeFromUser(DT_int_Arr, n);

    printPairsOfDateAndTime(DT_int_Arr,n);

    writeDatesToFile(DT_int_Arr, n);
}

