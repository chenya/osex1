#include "prog2_run.h"



void run_prog2(void)
{
    int num_of_pairs;
    
    struct DT_int DT_int_Arr[MAX_DT];
    struct DT_bits DT_bits_Arr[MAX_DT];

    num_of_pairs=getDT_intStructArrayFromFile(DT_int_Arr, DT_INT_FILE);

    printPairsOfDateAndTime(DT_int_Arr,num_of_pairs);

    createDT_bitsArrayFromDT_intArray(DT_int_Arr, DT_bits_Arr, num_of_pairs); 

    writeBitsToFile(DT_bits_Arr, num_of_pairs); 

   
}

