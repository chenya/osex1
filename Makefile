
OS := $(shell uname)
ifneq (,$(findstring CYGWIN_NT,$(OS)))
	EXT = .exe
else
	EXT =
endif


CC=gcc

PROG1=prog1$(EXT)
PROG2=prog2$(EXT)
PROG3=prog3$(EXT)

PROGS=$(PROG1) $(PROG2) $(PROG3)

CFLAGS=-c -Wall 
SHARED_FOLDER=shared
DT_OBJECTS=DT_files.o input_output.o DT_bits.o
OBJECTS_PROG1=prog1.o prog1_run.o $(DT_OBJECTS)
OBJECTS_PROG2=prog2.o prog2_run.o $(DT_OBJECTS)
OBJECTS_PROG3=prog3.o prog3_run.o sort.o $(DT_OBJECTS) 

all: $(PROGS)

prog1: $(OBJECTS_PROG1)
		$(CC) $(OBJECTS_PROG1) -o $(PROG1)

prog2: $(OBJECTS_PROG2)
		$(CC) $(OBJECTS_PROG2) -o $(PROG2)

prog3: $(OBJECTS_PROG3)
		$(CC) $(OBJECTS_PROG3) -o $(PROG3)

prog1.o: prog1.c
		$(CC) $(CFLAGS) prog1.c

prog1_run.o: prog1_run.c
		$(CC) $(CFLAGS) prog1_run.c

prog2.o: prog2.c
		$(CC) $(CFLAGS) prog2.c

prog2_run.o: prog2_run.c
		$(CC) $(CFLAGS) prog2_run.c		

prog3.o: prog3.c
		$(CC) $(CFLAGS) prog3.c

prog3_run.o: prog3_run.c
		$(CC) $(CFLAGS) prog3_run.c		

DT_bits.o: $(SHARED_FOLDER)/DT_bits.c
		$(CC) $(CFLAGS) $(SHARED_FOLDER)/DT_bits.c

DT_files.o: $(SHARED_FOLDER)/DT_files.c
		$(CC) $(CFLAGS) $(SHARED_FOLDER)/DT_files.c

input_output.o: $(SHARED_FOLDER)/input_output.c $(SHARED_FOLDER)/defs.h
		$(CC) $(CFLAGS) $(SHARED_FOLDER)/input_output.c

sort.o: $(SHARED_FOLDER)/sort.c
		$(CC) $(CFLAGS) $(SHARED_FOLDER)/sort.c	
clean:
		rm -vf *.o $(SHARED_FOLDER)/*.o *.log $(PROGS)

test:
	@echo going to run ex1_test.bash
	@echo 
	./ex1_test.bash
