#ifndef PROG3_RUN_H

#define PROG3_RUN_H
#include "shared/defs.h"
#include "shared/input_output.h"
#include "shared/DT_files.h"
#include "shared/sort.h"
void builPointerArray(struct DT_bits* array, struct DT_bits** pointer_array, int size);
void runProg3(void);


#endif /* end of include guard: PROG3_RUN_H */
