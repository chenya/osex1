#include "DT_bits.h"


/*
    return mask from 'lowBound' to 'highBound'
    lowHighMask(10,20) will return int with mask:
        01111111 11110000 00000000 00000000
*/
int lowHighMask(int lowBound, int highBound) 
{
    return ( (1<<((highBound)+1)) - (1<<(lowBound)) );
}

/*
    creeate mask for the position in 0-4 bits
*/
int dayMask(void)
{
    return lowHighMask(0,4);
}

/*
    creeate mask for the position in 5-8 bits
*/
int monthMask(void)
{
    return lowHighMask(5,8);
}


/*
    creeate mask for the position in 21-26 bits
*/
int minuteMask(void)
{
    return lowHighMask(21,26);
}

/*
    creeate mask for the position in 9-14 bits
*/
int yearMask(void)
{
    return lowHighMask(9,14);
}

/*
    creeate mask for the position in 15-20 bits
*/
int secondsMask(void)
{
    return lowHighMask(15,20);
}

/*
    creeate mask for the position in 27-31 bits
*/
int hourMask(void)
{
    return lowHighMask(27,31);
}

int getHourFromLittleEndian(int littleEndianNumber)
{
    //littleEndianNumber is signed, so first need to zero the last bit
    // and than shift to get the number
    //return  ( ( ((littleEndianNumber) >>1) & ( lowHighMask(26,30) ) ) >> 26 ) ;
    return  ( ((hourMask() & littleEndianNumber) >> 27) & 0x0000001f ) ;
}

int getDayFromLittleEndian(int littleEndianNumber)
{
    return (dayMask() & littleEndianNumber);
}

int getMonthFromLittleEndian(int littleEndianNumber)
{
    return ((monthMask() & littleEndianNumber) >> 5);
}

int getYearFromLittleEndian(int littleEndianNumber)
{
    return (((yearMask() & littleEndianNumber) >> 9) + 2000);
}

int getMinutesFromLittleEndian(int littleEndianNumber)
{
    return ((minuteMask() & littleEndianNumber) >> 21);
}


int getSecondsFromLittleEndian(int littleEndianNumber)
{
    return ((secondsMask() & littleEndianNumber) >> 15);
}

/*
	get an int number x, and change it to be in big endian form
	byte 0 --> byte 3
	byte 1 --> byte 2
	byte 2 --> byte 1
	byte 3 --> byte 0
*/
int littleEndianToBigEndian(int x)
{
    //return ( ((x>>24)& 0x000000ff )|((x<<8) & 0x00ff0000)|(((x>>8) & 0x0000ff00))|((x<<24)& 0xff000000 ));
    return ( ((x>>24) & 0x000000ff ) |
             ((x>>8)  & 0x0000ff00 ) |
             ((x<<8)  & 0x00ff0000 ) |
             ((x<<24) & 0xff000000 ));
}


//get in number and print as binary
// usefull for debugging
void printIntAsBinary(int num)
{
    unsigned int mask = MSB_SET(unsigned int); 
    int i;
    int byte_cout=1;
    for (i = 0; i < NUM_OF_BITS(num); i++) {
        if (num & mask) {
            printf("%d",1);
        } else {
            printf("%d",0);
        }
        if (!(byte_cout%8)) {
            printf(" ");
        }
        byte_cout++;    
        mask>>=1;
    }
    printf("\n");
}