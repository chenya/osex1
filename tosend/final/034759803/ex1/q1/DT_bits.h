#ifndef DT_BITS_H

#define DT_BITS_H

#include <stdio.h>

/*
struct DT_bits {
    int DT_little;
    int DT_big;
}; 
*/
#define NUM_OF_BITS(TYPE)   (sizeof(TYPE)*8)
#define MSB_SET(TYPE) ((TYPE)(1)<<(sizeof(TYPE)*8 - 1))

int lowHighMask(int lowBound, int highBound) ;

int dayMask(void);

int monthMask(void);

int minuteMask(void);


int yearMask(void);

 
int secondsMask(void);


int hourMask(void);


int getHourFromLittleEndian(int littleEndianNumber);

int getDayFromLittleEndian(int littleEndianNumber);

int getMonthFromLittleEndian(int littleEndianNumber);

int getYearFromLittleEndian(int littleEndianNumber);

int getMinutesFromLittleEndian(int littleEndianNumber);

int getSecondsFromLittleEndian(int littleEndianNumber);

int littleEndianToBigEndian(int littleEndianNumber);


void printIntAsBinary(int num);
#endif /* end of include guard: DT_BITS_H */
