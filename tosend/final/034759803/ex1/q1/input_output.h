#ifndef INPUT_OUTPUT_H

#define INPUT_OUTPUT_H


#include <stdio.h>
#include <stdlib.h>
#include "defs.h"
#include "DT_files.h"
#include "DT_bits.h"
//#include "bits.h"




int getNumberOfPairsFromUser(void);
//void getDateAndSecondsPairFromUser(int* days_since_year_2000, int* number_of_seconds_this_day);
void getDateAndSecondsPairFromUser(struct DT_int* dt_int);;
void fillDaysInEachMonth(int* days_in_month );
int extractMonthAndDays(int date_in_days, int* days_in_month, int* extract_month);
void transformDateInDaysToRealDate(int date_in_days, int* days_in_month, int* day, int* month, int* year);
void getDayMonthYear(int date_in_days, int* day, int* month, int* year);
void printActualDate(int date_in_days);
void getHourtMinuteSecond(int seconds, int* hour, int* minute, int* second);
void printActualTime(int seconds);
//void printActualDateAndTime(int date, int seconds);
void printActualDateAndTime(struct DT_int* dt_int);
void getPairsOfDateAndTimeFromUser(struct DT_int* DT_int_Arr, int num_of_pairs);

void printPairsOfDateAndTime(struct DT_int* DT_int_Arr, int num_of_pairs);

int littleEndianBitsDate(struct DT_int *dt_int);

void createDT_bitsArrayFromDT_intArray(struct DT_int *dt_int, struct DT_bits *dt_bits, int size);
void printLittleEndianAndBigEndianInHexa(struct DT_bits *dt_bits);
void printDT_bitsStructArray(struct DT_bits *dt_bits, int size);
void printDT_bits_Ptr_StructArray(struct DT_bits **dt_bits, int size);
int getHowToSortDT_bits_PtrArrayFromUser(void);
void printTheDateAndTimesAndHexaRepresantation(struct DT_bits *dt_bits);
void printLittleEndianActualDateAndTime(int littleEndian);
#endif /* end of include guard: INPUT_OUTPUT_H */
