#include "DT_files.h"

/*
 *    Intput: string which is path to a file, mod of the opening of file
 *
 *    Output: address of opening file by 'mod'
 *
 */
FILE* openFile(char* pathToFile, char* mode)
{
    FILE* file;

    file = fopen(pathToFile, mode); 
    
    CHECK_OPEN_FILE_EXIT_ON_ERROR(file, OPEN_FILE_FAILED_MESSAGE) ;


    return file;
}

/*
 *    Intput: string which is path to a file
 *
 *    Output: address to binary file, which open to WRITE
 *
 */
FILE* openBinFileToWrite(char* pathToFile)
{
    FILE *file;

    file=openFile(pathToFile, BINARY_WRITE);

    return file;
}

/*
 *    Intput: string which is path to a file
 *
 *    Output: address to binary file, which open to READ
 *
 */
FILE* openBinFileToRead(char* pathToFile)
{
    FILE *file;

    file=openFile(pathToFile, BINARY_READ);

    return file;
}

/*
 *    Intput: address to a file
 *
 *    Output: number represent if closing the file was success or a failure.
 *              0 - success
 *              otherwise failure
 */
int closeFile(FILE* file)
{

    IF_NULL(file) {
        return CLOSE_FILE_GET_NULL_PTR_ERROR;
    }

    return fclose(file);
}

/*
 *    Intput: address to a DT_int struct and adress to FILE
 *
 *    Output: if write successed --> the size has been write to file in bytes
 *            else --> number that is less than 1
 *
 */
int writeDT_intStructToBinFile(struct DT_int *dt_int, FILE* file_to_write)
{
     
    return writeStructToBinFileHelper(dt_int, sizeof(*dt_int), file_to_write);
}

/*
 *    Intput: address to a DT_bits struct and adress to FILE
 *
 *    Output: if write successed --> the size has been write to file in bytes
 *            else --> number that is less than 1
 *
 */
int writeDT_bitsStructToBinFile(struct DT_bits *dt_bits, FILE* file_to_write)
{
     
    return writeStructToBinFileHelper(dt_bits, sizeof(*dt_bits), file_to_write);
}

/*
 *    Intput: generic address, a size to write, address to FILE to write
 *
 *    Output: if write successed --> the size has been write to file in bytes
 *            else --> number that is less than 1
 *
 */
int writeStructToBinFileHelper(void* veriable_to_write, int size, FILE* file_to_write)
{
       return fwrite(veriable_to_write, size , 1, file_to_write ); 
}

/*
 *    Intput: address to a DT_int struct and adress to FILE
 *
 *    Output: if write successed --> the size has been read from file in bytes
 *            else --> number that is less than 1
 *
 */
int readDT_intStructFromBinFile(struct DT_int *dt_int, FILE* file_to_read)
{
    return readStructFromBinFileHelper(dt_int, sizeof(*dt_int), file_to_read);
}

/*
 *    Intput: address to a DT_int struct and adress to FILE
 *
 *    Output: if write successed --> the size has been read from file in bytes
 *            else --> number that is less than 1
 *
 */
int readDT_bitsStructFromBinFile(struct DT_bits *dt_bits, FILE* file_to_read)
{
    return readStructFromBinFileHelper(dt_bits, sizeof(*dt_bits), file_to_read);
}

/*
 *    Intput: generic address, a size to write, address to FILE to write
 *
 *    Output: if write successed --> the size has been read from file in bytes
 *            else --> number that is less than 1
 *
 */
int readStructFromBinFileHelper(void* veriable_to_read, int size, FILE* read_from_file)
{
       return fread(veriable_to_read, size, 1, read_from_file ); 
}

/*
 *    Intput: address to DT_int struct, num of dates that entered has input
 *
 *    Output: the ouput is to file 'DT_INT_FILE' - all the the DT_int's
 *
 */
void writeDatesToFile(struct DT_int* DT_int_arr, int num_of_dates)
{
    FILE *DT_file=openFile(DT_INT_FILE, BINARY_WRITE);
    int i;
    
    for (i = 0; i < num_of_dates; i++) {
        /* code */
        writeDT_intStructToBinFile(&(DT_int_arr[i]), DT_file);
    }

    closeFile( DT_file);

}

/*
 *    Intput: array of DT_bits structs and it's size
 *
 *    Output: to file 'DT_BITS_FILE' - the array of DT_bits struct
 *
 */
void writeBitsToFile(struct DT_bits* DT_bits_arr, int size)
{
    FILE *DT_file=openFile(DT_BITS_FILE, BINARY_WRITE);
    int i;
    
    for (i = 0; i < size; i++) {
        /* code */
        writeDT_bitsStructToBinFile(&(DT_bits_arr[i]), DT_file);
    }

    closeFile( DT_file);

}


/*
 *    Intput: address to struct DT_int, a string  which is a file path
 *
 *    Output: number of DT_int structs have been read from the file
 *
 */
int getDT_intStructArrayFromFile(struct DT_int *dt_int, char*  file_path)
{
    int i;
    FILE *read_file = openBinFileToRead(file_path);

    for (i=0; (i < MAX_DT) && !(feof(read_file)) ; i++) {
        readDT_intStructFromBinFile(&(dt_int[i]), read_file);               
    }

    closeFile(read_file);
    /*
     *  return the number of structs have been read to array
     */
    return i-1;
}


/*
 *    Intput: address to struct DT_bits, a string  which is a file path
 *
 *    Output: number of DT_bits structs have been read from the file
 *
 */
int getDT_bitsStructArrayFromFile(struct DT_bits *dt_bits, char*  file_path)
{
    int i;
    FILE *read_file = openBinFileToRead(file_path);

    for (i=0; (i < MAX_DT) && !(feof(read_file)) ; i++) {
        readDT_bitsStructFromBinFile(&(dt_bits[i]), read_file);               
    }

    closeFile(read_file);
    /*
     *  return the number of structs have been read to array
     */
    return i-1;
}
