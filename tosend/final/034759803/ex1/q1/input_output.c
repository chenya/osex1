#include "input_output.h"


/*
 *    Intput: Number of pairs(seconds from current date, days from year 2000)
 *
 *    Output: the number of pairs was entered as input
 *
 */
int getNumberOfPairsFromUser(void)
{
    int number_of_pairs;
    //printf("Enter Number of pairs numbers: ");
    scanf("%d",&number_of_pairs);
    return number_of_pairs;
}


/*
 *  Well... print New line('\n'). 
 */
void printNewLine(void)
{
    printf("\n");
}

/*
 *  what it function say...
 */
void getDateAndSecondsPairFromUser(struct DT_int* dt_int)
{
    scanf("%d %d",&(dt_int->date), &(dt_int->seconds));
}

/*
 *    Intput: empty array of  months
 *
 *    Output: array of mobths, filled of each month with days
 *              in days_in_months[JUNE] wiil be number of days 
 *              in month of june(JUNE_DAYS)
 *
 */
void fillDaysInEachMonth(int* days_in_month )
{
    days_in_month[JANUARY]=JANUARY_DAYS;
    days_in_month[FEBRUARY]=FEBRUARY_DAYS;
    days_in_month[MARCH]=MARCH_DAYS;
    days_in_month[APRIL]=APRIL_DAYS;
    days_in_month[MAY]=MAY_DAYS;
    days_in_month[JUNE]=JUNE_DAYS;
    days_in_month[JULY]=JULY_DAYS;
    days_in_month[AUGUST]=AUGUST_DAYS;
    days_in_month[SEPTEMBER]=SEPTEMBER_DAYS;
    days_in_month[OCTOBER]=OCTOBER_DAYS;
    days_in_month[NOVEMBER]=NOVEMBER_DAYS;
    days_in_month[DECEMBER]=DECEMBER_DAYS;
}

/*
 *    Intput: days since year 2000 array of days of each month, address of extracted month
 *
 *    Output: return the day of the month
 *
 */
int extractMonthAndDays(int date_in_days, int* days_in_month, int* extract_month)
{
    int days_in_current_year;
    int curr_month;
    int days_delta;

    days_in_current_year=date_in_days % DAYS_IN_YEAR ;
    days_delta = days_in_current_year;
    /*
     *  iterate through the months until there is days left to check wich are less than current month
     *   
     */
    for (curr_month = JANUARY; (curr_month < MONTHS_IN_YEAR) && ((days_delta - days_in_month[curr_month])   >= 0); curr_month++) {
        days_delta -= days_in_month[curr_month];
    }
 
    *extract_month = curr_month+1;
    return (days_delta==0) ? 1 : (days_delta+1);
}


void transformDateInDaysToRealDate(int date_in_days, int* days_in_month, int* day, int* month, int* year)
{
    *year=EXTRACT_YEAR(date_in_days);
    *day=extractMonthAndDays(date_in_days, days_in_month, month);
}

void getDayMonthYear(int date_in_days, int* day, int* month, int* year)
{
    int days_in_month[MONTHS_IN_YEAR];
    fillDaysInEachMonth(days_in_month);
    
    transformDateInDaysToRealDate(date_in_days, days_in_month, day, month, year);
}

/*
 *    Intput: number of days since year 2000
 *
 *    Output: printed the actual date which the date_in_days represent
 *
 */
void printActualDate(int date_in_days)
{
    int day, month, year;
    getDayMonthYear(date_in_days, &day, &month, &year);

    printf("%.2d/%.2d/%.2d",day,month,year);
}

/*
 *    Intput: seconds from begining of day, and hour, minute and second 
 *              to update
 *
 *    Output: there is no actual output. the hour minute and second will be
 *              update to actual value
 *
 */
void getHourtMinuteSecond(int seconds, int* hour, int* minute, int* second)
{
    *hour=*minute=*second=0;

    if (seconds != 0) {
        *hour+=EXTRACT_HOUR(seconds); 
        *minute+=(seconds - (*hour)*SECONDS_IN_HOUR ) / SECONDS_IN_MINUTE; 
        *second+=(seconds - (*hour)*SECONDS_IN_HOUR ) % SECONDS_IN_MINUTE; 
    }
    return ;
}

/*
 *  print the time represent by seconds from the beginning of day
 */
void printActualTime(int seconds)
{
    int hour;
    int minute;
    int second;
    
    
    getHourtMinuteSecond(seconds, &hour, &minute, &second);
    
    printf("%.2d:%.2d:%.2d",hour,minute,second);
}

/*
 *  print the date and date from DT_int struct
 */
void printActualDateAndTime(struct DT_int* dt_int)
{
    printActualDate(dt_int->date); 
    printf(" ");
    printActualTime(dt_int->seconds); 
}

/*
 *    Intput: array of DT_int of structs and it's size(the num of of pairs...)
 *
 *    Output: Non. the function fill thee array with the given by the user
 *
 */

void getPairsOfDateAndTimeFromUser(struct DT_int* DT_int_Arr, int num_of_pairs)
{
    int i;

    for (i = 0; i < num_of_pairs; i++) {
        getDateAndSecondsPairFromUser(&(DT_int_Arr[i]));
    }
}

void printPairsOfDateAndTime(struct DT_int* DT_int_Arr, int num_of_pairs)
{
    int i;
    for (i = 0; i < num_of_pairs; i++) {
        printActualDateAndTime(&(DT_int_Arr[i])) ;
        printNewLine();
    }
    
}


/*
 *    Intput: address to DT_int  struct
 *
 *    Output: the little endian format of the time data
 *              in the struct
 *
 */

int littleEndianBitsDate(struct DT_int *dt_int)
{
    int little;
    int hour, minute,second;
    int day, month, year; 
    //int more_days;

    getDayMonthYear(dt_int->date, &day, &month, &year);
    getHourtMinuteSecond(dt_int->seconds, &hour, &minute, &second);
    
    
    little = 0;
    little |= hour;
    little <<=6;
    little |= minute;
    little <<=6;
    little |= second;
    little <<=6;
    little |= (year-2000);
    little <<=4;
    little |= month;
    little <<=5;
    little |= day;

    return little;
}


/*
 *    Intput: array of DT_int and array of DT_bits and the size of the array(both of them with same size)
 *
 *    Output: None. the function create the DT_bits array.
 *
 */
void createDT_bitsArrayFromDT_intArray(struct DT_int *dt_int, struct DT_bits *dt_bits, int size)
{
   int i;
   for (i = 0; i < size; i++) {
       (dt_bits[i]).DT_little=littleEndianBitsDate(&(dt_int[i]));
       (dt_bits[i]).DT_big=littleEndianToBigEndian((dt_bits[i]).DT_little);
   }
}

void printLittleEndianActualDateAndTime(int littleEndian)
{
    printf("%.2d/%.2d/%.2d ", getDayFromLittleEndian(littleEndian), 
                        getMonthFromLittleEndian(littleEndian), 
                        getYearFromLittleEndian(littleEndian));
    printf("%.2d:%.2d:%.2d",
                        getHourFromLittleEndian(littleEndian), 
                        getMinutesFromLittleEndian(littleEndian), 
                        getSecondsFromLittleEndian(littleEndian));
}

void printLittleEndianAndBigEndianInHexa(struct DT_bits *dt_bits)
{
    printf("%.8x %.8x", dt_bits->DT_little,dt_bits->DT_big);
}


void printTheDateAndTimesAndHexaRepresantation(struct DT_bits *dt_bits)
{
    printLittleEndianActualDateAndTime(dt_bits->DT_little);
    printf("\t\t");
    printLittleEndianAndBigEndianInHexa(dt_bits);
}


void printDT_bitsStructArray(struct DT_bits *dt_bits, int size)
{
    int i;

    for (i = 0; i < size; i++) {
        printLittleEndianAndBigEndianInHexa(&(dt_bits[i]));
        printNewLine();
    }
}
void printDT_bits_Ptr_StructArray(struct DT_bits **dt_bits, int size)
{
        int i;

    for (i = 0; i < size; i++) {
        //printLittleEndianAndBigEndianInHexa((dt_bits[i]));
        printTheDateAndTimesAndHexaRepresantation(dt_bits[i]);
        printNewLine();
    }
}
int getHowToSortDT_bits_PtrArrayFromUser(void)
{
    int how_to_sort_the_array;
    //printf("How to sort DT_bits_Ptr_Arr array?: ");
    scanf("%d",&how_to_sort_the_array);
    
    return how_to_sort_the_array;
}

