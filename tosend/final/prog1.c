/*
 *	Date: 19.11.2015
 *
 *	Name: Chen Yachin
 *	
 *	ID: 034759803
 *
 *	Description: 
 *
 */
#include "prog1.h"

/*
 *	Intput: 1. number which represent the number of pairs which will entered
 *			2. 	days from year 2000 and second of day
 *
 *	Output: the actual date and time that was given
 *
 */
void runProg1(void)
{
    int n;
    
    struct DT_int DT_int_Arr[MAX_DT];

    n=getNumberOfPairsFromUser();
    getPairsOfDateAndTimeFromUser(DT_int_Arr, n);

    printPairsOfDateAndTime(DT_int_Arr,n);

    writeDatesToFile(DT_int_Arr, n);
}


int main(void)
{
    runProg1();

    return 0;
}
