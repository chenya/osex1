/*
 *    Date: 19.11.2015
 *
 *    Name: Chen Yachin
 *    
 *    ID: 034759803
 *
 *    Description: 
 *
 */
#include "prog3.h"


/*
 *    Intput: array of DT_bits struct, array of pinters to DT_bits struct, and size of both arrays
 *
 *    Output: None. the function build the pointer array by put the address of each sell of the array
 *                  of DT_bits struct in correct sell at pointer array
 *
 */
void builPointerArray(struct DT_bits* array, struct DT_bits** pointer_array, int size)
{
    int i;
    for (i = 0; i < size; i++) {
        pointer_array[i]=&(array[i]);
    }
}

/*
 *    Intput: None.
 *
 *    Output: printed sorted values from the binary file  which found in 'DT_BITS_FILE'
 *
 */
void runProg3(void)
{
    int num_of_dt_bits;
    struct DT_bits DT_bits_Arr[MAX_DT];//array of struct
    struct DT_bits* DT_bits_Ptr_Arr[MAX_DT];// array of pointers to struct
    int how_to_sort;
    num_of_dt_bits=getDT_bitsStructArrayFromFile(DT_bits_Arr,DT_BITS_FILE);

    builPointerArray(DT_bits_Arr, DT_bits_Ptr_Arr, num_of_dt_bits);
    

    //printDT_bitsStructArray(DT_bits_Arr, num_of_dt_bits);
    how_to_sort=getHowToSortDT_bits_PtrArrayFromUser();
    sortBitsStructPointerArray(DT_bits_Ptr_Arr, num_of_dt_bits, how_to_sort);
    printDT_bits_Ptr_StructArray(DT_bits_Ptr_Arr,num_of_dt_bits);
    
}

int main(void)
{
    runProg3();
    return 0;
}
