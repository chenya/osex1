#ifndef DT_FILES_H

#define DT_FILES_H


#include <stdio.h>
#include <stdlib.h>
#include "defs.h"

/*
 *  CONSTANTS
 */


#define OPEN_FILE_FAILED_MESSAGE  "Failed to Open File"

#define CLOSE_FILE_GET_NULL_PTR_ERROR (-2)

/*
 * MACROS
 */

#define IF_NULL(F)    if ((F) == NULL) 
#define EXIT_ON_ERROR(S) \
        fprintf(stderr,(S)); \
        putchar('\n');  \
        exit(1);    


#define CHECK_OPEN_FILE_EXIT_ON_ERROR(F,S) \
    IF_NULL(F) { \
        EXIT_ON_ERROR(S) \
    }


/*
 *  PROTOTYPES
 */

FILE* openFile(char* pathToFile, char* mode);
FILE* openBinFileToWrite(char* pathToFile);
FILE* openBinFileToRead(char* pathToFile);


int writeStructToBinFile(void* veriable_to_write, FILE* file_to_write);
int writeStructToBinFileHelper(void* veriable_to_write, int size, FILE* file_to_write);

int readStructFromBinFile(void* veriable_to_write, FILE* file_to_write);
int readStructFromBinFileHelper(void* veriable_to_write, int size, FILE* read_from_file);

int closeFile(FILE* file);


int writeDT_intStructToBinFile(struct DT_int *dt_int, FILE* file_to_write);
int readDT_intStructFromBinFile(struct DT_int *dt_int, FILE* file_to_read);

int writeDT_bitsStructToBinFile(struct DT_bits *dt_bits, FILE* file_to_write);
int readDT_bitsStructFromBinFile(struct DT_bits *dt_bits, FILE* file_to_read);
void writeDatesToFile(struct DT_int* DT_int_arr, int num_of_dates);
int getDT_intStructArrayFromFile(struct DT_int *dt_int, char*  file_path);

void writeBitsToFile(struct DT_bits* DT_bits_arr, int size);
int getDT_bitsStructArrayFromFile(struct DT_bits *dt_bits, char*  file_path);

#endif /* end of include guard: DT_FILES_H */
