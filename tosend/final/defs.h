#ifndef DEFS_H

#define DEFS_H

//#include "DT_int.h"
//#include "DT_bits.h"


#define MAX_DT 20
#define DT_INT_FILE "date_time_int.txt"
#define DT_BITS_FILE "date_time_bits.txt"

#define MILLENIUM 2000
#define DAYS_IN_YEAR 365

#define SECONDS_IN_HOUR 3600
#define SECONDS_IN_MINUTE 60

#define BINARY_READ  "rb"
#define BINARY_WRITE "wb"

#define EXTRACT_YEAR(Y) ( MILLENIUM + ((Y) / DAYS_IN_YEAR))


#define EXTRACT_HOUR_FROM_SECONDS(S) ( ((S) / SECONDS_IN_HOUR))

#define EXTRACT_MINUTE_FROM_SECONDS(S) ( (iEXTRACT_HOUR_FROM_SECONDS(M) / SECONDS_IN_HOUR))
#define EXTRACT_HOUR(H) ( ((H) / SECONDS_IN_HOUR))

#define LITTLE_ENDIAN_TO_BIG_ENDIAN(N) (  ((N)>>24)|(((N) <<8) &0x00ff0000)|((((N)>>8) &0x0000ff00))|((N) << 24) )

#define SORT_BY_MONTH   1
#define SORT_BY_YEAR    2
#define SORT_BY_SECOND  3

#define SECONDS_IN_DAY 86400

typedef enum {
    JANUARY=0, 
    FEBRUARY, 
    MARCH,
    APRIL,
    MAY,
    JUNE,
    JULY,
    AUGUST,
    SEPTEMBER,
    OCTOBER,
    NOVEMBER,
    DECEMBER,
    MONTHS_IN_YEAR
} Months; 

typedef enum {
    JANUARY_DAYS=31, 
    FEBRUARY_DAYS=28, 
    MARCH_DAYS=31,
    APRIL_DAYS=30,
    MAY_DAYS=31,
    JUNE_DAYS=30,
    JULY_DAYS=31,
    AUGUST_DAYS=31,
    SEPTEMBER_DAYS=30,
    OCTOBER_DAYS=31,
    NOVEMBER_DAYS=30,
    DECEMBER_DAYS=31
} MonthDays ;

typedef enum {
    FALSE,
    TRUE
}BOOL;


typedef struct DT_int {
    int date;
    int seconds;
} DT_Int;


typedef struct DT_bits {
    int DT_little;
    int DT_big;
} DT_Bits; 

#endif /* end of include guard: DEFS_H */
