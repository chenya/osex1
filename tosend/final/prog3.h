#ifndef PROG3_H

#define PROG3_H
#include "defs.h"
#include "input_output.h"
#include "DT_files.h"
#include "DT_sort.h"
void builPointerArray(struct DT_bits* array, struct DT_bits** pointer_array, int size);
void runProg3(void);


#endif /* end of include guard: PROG3_RUN_H */
