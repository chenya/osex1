#!/usr/bin/bash

ME="Chen Yachin, id 034759803"
STR1="Ex#1, prog1 "
STR2="Ex#1, prog2 "
STR3="Ex#1, prog3 "


echo running prog1...
 
(echo $STR1$ME;./prog1 << end_1;echo) &> prog1.log
8
12 793
396 7260
761 7329
5555 5555
806 86000
1420 85999
372 3661
31 7200
end_1

echo running prog2...
(echo $STR2$ME;./prog2;echo) >& prog2.log

echo running prog3 with input 1 ...
(echo $STR3$ME;./prog3 << end_3a;echo) &> prog3a.log
1
end_3a

echo running prog3 with input 2 ...
(echo $STR3$ME;./prog3 << end_3b;echo) &> prog3b.log
2
end_3b

echo running prog3 with input 3 ...
(echo $STR3$ME;./prog3 << end_3c;echo) &> prog3c.log
3
end_3c

echo creating a single test.log file
cat prog1.log prog2.log prog3a.log prog3b.log prog3c.log > test.log
