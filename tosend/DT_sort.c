#include "DT_sort.h"

/*
 *    Intput: Two DT_int struct
 *
 *    Output: 1 --> month in struct 1 bigger than month in struct 2
 *           -1 --> month in struct 1 smaller than month in struct 2
 *            0 --> month in struct 1 equal to month in struct 2
 */
int compare_by_month(const struct DT_bits* dt_bits1, const struct DT_bits* dt_bits2)
{
    int month1, month2;
    
    month1 = getMonthFromLittleEndian(dt_bits1->DT_little);
    month2 = getMonthFromLittleEndian(dt_bits2->DT_little);
    if (month1 < month2) return -1;
    if (month1 > month2) return 1;
    
    return 0;
}

/*
 *    Intput: Two DT_int struct
 *
 *    Output: 1 --> year in struct 1 bigger than year in struct 2
 *           -1 --> year in struct 1 smaller than year in struct 2
 *            0 --> year in struct 1 equal to year in struct 2
 */
int compare_by_year(const struct DT_bits* dt_bits1, const struct DT_bits* dt_bits2)
{
    int year1, year2;
    
    year1 = getYearFromLittleEndian(dt_bits1->DT_little);
    year2 = getYearFromLittleEndian(dt_bits2->DT_little);
    if (year1 < year2) return -1;
    if (year1 > year2) return 1;
    
    return 0;
}

/*
 *    Intput: Two DT_int struct
 *
 *    Output: 1 --> second in struct 1 bigger than second in struct 2
 *           -1 --> second in struct 1 smaller than second in struct 2
 *            0 --> second in struct 1 equal to second in struct 2
 */
int compare_by_second(const struct DT_bits* dt_bits1, const struct DT_bits* dt_bits2)
{
    int seconds1, seconds2;
    
    seconds1 = getSecondsFromLittleEndian(dt_bits1->DT_little);
    seconds2 = getSecondsFromLittleEndian(dt_bits2->DT_little);
    if (seconds1 < seconds2) return -1;
    if (seconds1 > seconds2) return 1;
    
    return 0;
}


int compareDT_bitsStructByMonth(const void* dt_bits1, const void* dt_bits2)
{
    return compare_by_month(* ( struct DT_bits* const *) dt_bits1, * ( struct DT_bits* const *) dt_bits2 );
}

int compareDT_bitsStructByYear(const void* dt_bits1, const void* dt_bits2)//get pointers to a pointers
{
    return compare_by_year( * ( struct DT_bits* const *) dt_bits1, * ( struct DT_bits* const *) dt_bits2 );
}

int compareDT_bitsStructBySecond(const void* dt_bits1, const void* dt_bits2)
{
    return compare_by_second( * ( struct DT_bits* const *) dt_bits1, * ( struct DT_bits* const *) dt_bits2 );
}


/*
 *    Intput: array of pointers to DT_bits struct, the size of this array and how the array will br sorted
 *
 *    Output: None. the function will sort the array by given number
 *               *the qsort library function used for sorting
 *
 */
void sortBitsStructPointerArray(struct DT_bits** dt_ptr_bits, int size, int how_to_sort)
{
    if (how_to_sort == SORT_BY_MONTH) {
        qsort(dt_ptr_bits, size, sizeof(struct DT_bits*), compareDT_bitsStructByMonth );
    } else if (how_to_sort == SORT_BY_YEAR) {
        qsort(dt_ptr_bits, size, sizeof(struct DT_bits*), compareDT_bitsStructByYear );
    } else if (how_to_sort == SORT_BY_SECOND) {
        qsort(dt_ptr_bits, size, sizeof(struct DT_bits*), compareDT_bitsStructBySecond );
    } else {
        printf("Invlid parameter for sorting");
    }     
}
