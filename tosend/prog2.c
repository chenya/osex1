/*
 *	Date: 19.11.2015
 *
 *	Name: Chen Yachin
 *	
 *	ID: 034759803
 *
 *	Description: 
 *
 */
#include "prog2.h"

/*
 *	Intput: None. the data retreive from a file, not the stdin
 *
 *	Output: output is going in two direction: 
 *			1. to file, represent by 'DT_INT_FILE'
 *			2. to screen(stdout).
 *
 */
void runProg2(void)
{
    int num_of_pairs;
    
    struct DT_int DT_int_Arr[MAX_DT];
    struct DT_bits DT_bits_Arr[MAX_DT];

    num_of_pairs=getDT_intStructArrayFromFile(DT_int_Arr, DT_INT_FILE);

    printPairsOfDateAndTime(DT_int_Arr,num_of_pairs);

    createDT_bitsArrayFromDT_intArray(DT_int_Arr, DT_bits_Arr, num_of_pairs); 

    writeBitsToFile(DT_bits_Arr, num_of_pairs); 

   
}


int main(void)
{
    
    runProg2();
    
    return 0;
}

