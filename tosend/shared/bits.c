//#include "bits.h"
#include <stdio.h>
#include <stdlib.h>
//#include "input_output.h"

#define LSB_MASK(TYPE) ((TYPE)1)
#define LSB8_MASK(TYPE) ((TYPE)0xFF)
#define LSB4_MASK(TYPE) ((TYPE)0xF)
#define ALL_1_MASK(TYPE) (~(TYPE)(0))
#define BIT_I_SET(I, TYPE) ((TYPE)(1) << (I))
#define BIT_I_CLEAR(I, TYPE) (~(TYPE)(1) << (I))
#define MSB_SET(TYPE) ((TYPE)(1)<<(sizeof(TYPE)*8 - 1))
#define NUM_OF_BITS(M)   (sizeof(M)*8)

#define MSB8_MASK(TYPE) (LSB8_MASK(TYPE) << ( (sizeof(TYPE)-1)*8) )
#define MSB4_MASK(TYPE) (LSB4_MASK(TYPE) << ( (sizeof(TYPE))*8-4) )

/*
#define LITTLE_ENDIAN_TO_BIG_ENDIAN(N) (  ((N)>>24)|(((N) <<8) &0x00ff0000)|((((N)>>8) &0x0000ff00))|((N) << 24) )

#define LOWER_UPPER_BOUND_MASK(LOWER,UPPER)  ( (1<<((UPPER)+1)) - (1<<(LOWER)) )

#define DAY_MASK   (LOWER_UPPER_BOUND_MASK(0,4))
#define MONTH_MASK   (LOWER_UPPER_BOUND_MASK(5,8))
#define YEAR_MASK   (LOWER_UPPER_BOUND_MASK(9,14))
#define SECOND_MASK   (LOWER_UPPER_BOUND_MASK(15,20))
#define MINUTE_MASK   (LOWER_UPPER_BOUND_MASK(21,26))

#define HOUR_MASK   (LOWER_UPPER_BOUND_MASK(27,31))
#define HOUR_MASK_HELPER   (LOWER_UPPER_BOUND_MASK(26,30))
*/
/*
#define getDayFromLittleEndian(little)      ((DAY_MASK)  &(little))
#define getMonthFromLittleEndian(little)    ( ( (MONTH_MASK)  &(little) )>>5  )
#define getYearFromLittleEndian(little)     ( ( ( (YEAR_MASK)  &(little) )>>9) + 2000)
//#define getHourFromLittleEndian(little)     ( ( (((HOUR_MASK)  &(little) )>>1)& HOUR_MASK_HELPER ) >> 26)
#define getMinutesFromLittleEndian(little)     ( ( (MINUTE_MASK)   &(little) )>>21 )
//#define getSecondsFromLittleEndian(little)  ( ( (SECOND_MASK)&(little) )>>15  )
*/
/*
int getHourFromLittleEndian( unsigned int  little)
{
    return ( (little) >>27 );
}
*/


/*
    return mask from 'lowBound' to 'highBound'
    lowHighMask(10,20) will return int with mask:
        01111111 11110000 00000000 00000000
*/
int lowHighMask(int lowBound, int highBound) 
{
    return ( (1<<((highBound)+1)) - (1<<(lowBound)) );
}

/*
    creeate mask for the position in 0-4 bits
*/
int dayMask(void)
{
    return lowHighMask(0,4);
}

/*
    creeate mask for the position in 5-8 bits
*/
int monthMask(void)
{
    return lowHighMask(5,8);
}




/*
    creeate mask for the position in 21-26 bits
*/
int minuteMask(void)
{
    return lowHighMask(21,26);
}

/*
    creeate mask for the position in 9-14 bits
*/
int yearMask(void)
{
    return lowHighMask(9,14);
}

/*
    creeate mask for the position in 15-20 bits
*/
int secondsMask(void)
{
    return lowHighMask(15,20);
}

/*
    creeate mask for the position in 27-31 bits
*/
int hourMask(void)
{
    return lowHighMask(27,31);
}

/*
int getHourFromLittleEndian(int n)
{
    //return  ( ( ((n) >>1) & ( hourMaskHelper() ) ) >> 26 ) ;
    return  ( ( ((n) >>1) & ( lowHighMask(26,30) ) ) >> 26 ) ;
}*/


int getHourFromLittleEndian(int littleEndianNumber)
{
    
    return  ( ( ((littleEndianNumber) >>1) & ( lowHighMask(26,30) ) ) >> 26 ) ;
}

int getDayFromLittleEndian(int littleEndianNumber)
{
    return (dayMask() & littleEndianNumber);
}

int getMonthFromLittleEndian(int littleEndianNumber)
{
    return ((monthMask() & littleEndianNumber) >> 5);
}

int getYearFromLittleEndian(int littleEndianNumber)
{
    return (((yearMask() & littleEndianNumber) >> 14) + 2000);
}

int getMinutesFromLittleEndian(int littleEndianNumber)
{
    return ((minuteMask() & littleEndianNumber) >> 21);
}


int getSecondsFromLittleEndian(int littleEndianNumber)
{
    return ((minuteMask() & littleEndianNumber) >> 15);
}

int littleEndianToBigEndian(int x)
{
    return ((x>>24)|((x<<8) &0x00ff0000)|(((x>>8) &0x0000ff00))|(x << 24));
}

//#define getDayFromLittleEndian(little)      ((DAY_MASK)  &(little))
//#define getMonthFromLittleEndian(little)    ( ( (MONTH_MASK)  &(little) )>>5  )
//#define getYearFromLittleEndian(little)     ( ( ( (YEAR_MASK)  &(little) )>>9) + 2000)
//#define getHourFromLittleEndian(little)     ( ( (((HOUR_MASK)  &(little) )>>1)& HOUR_MASK_HELPER ) >> 26)
//#define getMinutesFromLittleEndian(little)     ( ( (MINUTE_MASK)   &(little) )>>21 )
//#define getSecondsFromLittleEndian(little)  ( ( (SECOND_MASK)&(little) )>>15  )


void printIntAsBinary(int num);
     

void printIntAsBinary(int num)
{
    unsigned int mask = MSB_SET(unsigned int); 
    int i;
    int byte_cout=1;
    for (i = 0; i < NUM_OF_BITS(num); i++) {
        if (num & mask) {
            printf("%d",1);
        } else {
            printf("%d",0);
        }
        if (!(byte_cout%8)) {
            printf(" ");
        }
        byte_cout++;    
        mask>>=1;
    }
    printf("\n");
}





int main(void)
{
    int i;
    int days;
    int seconds;
    int bit_date=0x7b3bb071;
    int big_date=0x5511f62e;
    unsigned int mask;
    days=761;
    seconds=7329;

    int hour, minute,second;
    int day, month, year; 

    //getDayMonthYear(days, &day, &month, &year);
   // getHourtMinuteSecond(seconds, &hour, &minute, &second);
    
 
    printf("---------------------\n");
    //printIntAsBinary(bit_date);
    //printIntAsBinary(littleEndianToBigEndian(bit_date));

    //printIntAsBinary(MSB4_MASK(int));

    //printf("little endian: %x\n",bit_date);
    //printf("big endian: %x\n", littleEndianToBigEndian(bit_date) ); 
    
    //printf("big endian MACRO:%x\n",LITTLE_ENDIAN_TO_BIG_ENDIAN(bit_date) );
    printf("MONTH MASK:\n");
    printf("%x\n",(bit_date));
    printIntAsBinary(bit_date);
    printIntAsBinary(lowHighMask(27,31));
    //printIntAsBinary(getHourFromLittleEndian());
    printIntAsBinary(getYearFromLittleEndian(bit_date));
    printf("%d\n",getYearFromLittleEndian(bit_date));
    
    //printIntAsBinary(((HOUR_MASK&bit_date)>>27));
    //printIntAsBinary(getDayFromLittleEndian((bit_date)) );
    //printf("%d/%d/%d\t",getDayFromLittleEndian(bit_date), getMonthFromLittleEndian(bit_date), getYearFromLittleEndian(bit_date));
    //printf("%.2d:%.2d:%.2d\n",getHourFromLittleEndian(bit_date), getMinutesFromLittleEndian(bit_date), getSecondsFromLittleEndian(bit_date));
    return 0;
}
