#include "files.h"



FILE* openFile(char* pathToFile, char* mode)
{
    FILE* file;

    file = fopen(pathToFile, mode); 
    
    CHECK_OPEN_FILE_EXIT_ON_ERROR(file, OPEN_FILE_FAILED_MESSAGE) ;


    return file;


}

FILE* openBinFileToWrite(char* pathToFile)
{
    FILE *file;

    file=openFile(pathToFile, BINARY_WRITE);

    return file;
}


FILE* openBinFileToRead(char* pathToFile)
{
    FILE *file;

    file=openFile(pathToFile, BINARY_READ);

    return file;
}

int closeFile(FILE* file)
{

    IF_NULL(file) {
        return CLOSE_FILE_GET_NULL_PTR_ERROR;
    }

    return fclose(file);
}


int dummySize(void)
{
    return sizeof(DUMMY);
}


int writeStructToBinFile(void* veriable_to_write, FILE* file_to_write)
{
    return writeStructToBinFileHelper(veriable_to_write, dummySize, file_to_write);
}

int writeStructToBinFileHelper(void* veriable_to_write, int (*sizeFunc)(void), FILE* file_to_write)
{
       return fwrite(veriable_to_write, sizeFunc(), 1, file_to_write ); 
}


int readStructFromBinFile(void* veriable_to_write, FILE* file_to_write)
{
    return readStructFromBinFileHelper(veriable_to_write, dummySize, file_to_write);
}

int readStructFromBinFileHelper(void* veriable_to_write, int (*sizeFunc)(void), FILE* read_from_file)
{
       return fread(veriable_to_write, sizeFunc(), 1, read_from_file ); 
}
