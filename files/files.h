#ifndef FILES_H

#define FILES_H




#include <stdio.h>
#include <stdlib.h>


#define BINARY_READ  "rb"
#define BINARY_WRITE "wb"

#define OPEN_FILE_FAILED_MESSAGE  "Failed to Open File"

#define CLOSE_FILE_GET_NULL_PTR_ERROR (-2)

/*
 * MACROS
 */

#define IF_NULL(F)    if ((F) == NULL) 
#define EXIT_ON_ERROR(S) \
        fprintf(stderr,(S)); \
        putchar('\n');  \
        exit(1);    


#define CHECK_OPEN_FILE_EXIT_ON_ERROR(F,S) \
    IF_NULL(F) { \
        EXIT_ON_ERROR(S) \
    }


typedef struct _dummy
{
    int a;
    int b;

}DUMMY;


/*
 *  PROTOTYPES
 */

FILE* openFile(char* pathToFile, char* mode);
FILE* openBinFileToWrite(char* pathToFile);
FILE* openBinFileToRead(char* pathToFile);


int writeStructToBinFile(void* veriable_to_write, FILE* file_to_write);
int writeStructToBinFileHelper(void* veriable_to_write, int (*sizeFunc)(void), FILE* file_to_write);

int readStructFromBinFile(void* veriable_to_write, FILE* file_to_write);
int readStructFromBinFileHelper(void* veriable_to_write, int (*sizeFunc)(void), FILE* read_from_file);

int dummySize(void);

int closeFile(FILE* file);


#endif /* end of include guard: FILES_H */
