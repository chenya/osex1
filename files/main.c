#include "files.h"

int main()
{
    DUMMY d;
    DUMMY e1;
    DUMMY e2;

    FILE *f;

    d.a=-1;
    d.b=(~(-4));

    f=openFile("test.bin", BINARY_WRITE);
    writeStructToBinFile(&d, f);
    d.a=51;
    d.b=101;
    writeStructToBinFile(&d, f);
    closeFile(f);

    
    f=openFile("test.bin", BINARY_READ);


    readStructFromBinFile(&e1, f);
    readStructFromBinFile(&e2, f);

    printf("e1.a ==> %d, e1.b ==> %d\n", e1.a, e1.b);
    printf("e2.a ==> %d, e2.b ==> %d\n", e2.a, e2.b);
    
    closeFile(f);

    

    return 0;
}
