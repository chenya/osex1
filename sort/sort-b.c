#include <stdio.h>
#include <stdlib.h>
#include <string.h>


    
int
compareInt(const int* a, const int* b)
{
    if (*a > *b) return 1; 
    if (*a < *b) return -1; 
    if (*a == *b) return 0; 
}

int
cmpint(const void *p1, const void *p2)
{
    /* The actual arguments to this function are "pointers to
     *               pointers to char", but strcmp(3) arguments are "pointers
     *                             to char", hence the following cast plus dereference */

    return compareInt((const int *) p1,  ( const int *) p2);
}

int
main(int argc, char *argv[])
{
    int i;

    int arr[10] = {6,4,10,2,4,8,2,4,7,9};

    for (i = 0; i < 10; i++)
        printf("%d ",arr[i]);
    printf("\n");
    qsort(arr, 10, sizeof(int ), cmpint );

    for (i = 0; i < 10; i++)
        printf("%d ",arr[i]);
    printf("\n");
    exit(EXIT_SUCCESS);
}
