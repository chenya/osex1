#include "prog3_run.h"


void builPointerArray(struct DT_bits* array, struct DT_bits** pointer_array, int size)
{
    int i;
    for (i = 0; i < size; i++) {
        pointer_array[i]=&(array[i]);
    }
}

void runProg3(void)
{
    int num_of_dt_bits;
    struct DT_bits DT_bits_Arr[MAX_DT];//array of struct
    struct DT_bits* DT_bits_Ptr_Arr[MAX_DT];// array of pointers to struct
    int how_to_sort;
    num_of_dt_bits=getDT_bitsStructArrayFromFile(DT_bits_Arr,DT_BITS_FILE);

    builPointerArray(DT_bits_Arr, DT_bits_Ptr_Arr, num_of_dt_bits);
    

    //printDT_bitsStructArray(DT_bits_Arr, num_of_dt_bits);
    how_to_sort=getHowToSortDT_bits_PtrArrayFromUser();
    sortBitsStructPointerArray(DT_bits_Ptr_Arr, num_of_dt_bits, how_to_sort);
    printDT_bits_Ptr_StructArray(DT_bits_Ptr_Arr,num_of_dt_bits);
    
}

