#!/usr/bin/bash
echo running prog1...
./prog1 << end_1 &> prog1.log
8
12 793
396 7260
761 7329
22222 22222
806 86000
1420 85999
372 3661
31 7200
end_1

echo running prog2...
./prog2 >& prog2.log

echo running prog3 with input 1 ...
./prog3 << end_3a &> prog3a.log
1
end_3a

echo running prog3 with input 2 ...
./prog3 << end_3b &> prog3b.log
2
end_3b

echo running prog3 with input 3 ...
./prog3 << end_3c &> prog3c.log
3
end_3c

echo creating a single test.log file
cat prog1.log prog2.log prog3a.log prog3b.log prog3c.log > test.log
