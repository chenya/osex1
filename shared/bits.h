/*
#define LSB_MASK(TYPE) ((TYPE)1)
#define LSB8_MASK(TYPE) ((TYPE)0xFF)
#define ALL_1_MASK(TYPE) (!(TYPE)(0))
#define BIT_I_SET(I, TYPE) ((TYPE)(1) << (I))
#define BIT_I_CLEAR(I, TYPE) (~(TYPE)(1) << (I))
#define MSB_SET(TYPE) ((TYPE)(1)<<(sizeof(TYPE)*8 - 1))
#define NUM_OF_BITS(TYPE)   (sizeof(TYPE)*8)
#define MSB8_MASK(TYPE) (LSB8_MASK(TYPE) << ( (sizeof(TYPE)-1)*8) )
#define MSB4_MASK(TYPE) (LSB4_MASK(TYPE) << ( (sizeof(TYPE))*8-4) )

#define LOWER_UPPER_BOUND_MASK(LOWER,UPPER)  ( (1<<((UPPER)+1)) - (1<<(LOWER)) )

#define DAY_MASK   (LOWER_UPPER_BOUND_MASK(0,4))
#define MONTH_MASK   (LOWER_UPPER_BOUND_MASK(5,8))
#define YEAR_MASK   (LOWER_UPPER_BOUND_MASK(9,14))
#define SECOND_MASK   (LOWER_UPPER_BOUND_MASK(15,20))
#define MINUTE_MASK   (LOWER_UPPER_BOUND_MASK(21,26))
#define HOUR_MASK   (LOWER_UPPER_BOUND_MASK(27,31))
#define HOUR_MASK_HELPER   (LOWER_UPPER_BOUND_MASK(26,30))

#define getDayFromLittleEndian(little)      ((DAY_MASK)  &(little))
#define getMonthFromLittleEndian(little)    ( ( (MONTH_MASK)   &(little) )>>5  )
#define getYearFromLittleEndian(little)     ( ( ( (YEAR_MASK)  &(little) )>>9) + 2000)
#define getHourFromLittleEndian(little)     ( ( (((HOUR_MASK)  &(little) )>>1)& HOUR_MASK_HELPER ) >> 26)
#define getMinutesFromLittleEndian(little)  ( ( (MINUTE_MASK)  &(little) )>>21 )
#define getSecondsFromLittleEndian(little)  ( ( (SECOND_MASK)  &(little) )>>15  )
*/
int lowHighMask(int lowBound, int highBound) ;

/*
    creeate mask for the position in 0-4 bits
*/
int dayMask(void);

int monthMask(void);

int minuteMask(void);


int yearMask(void);

 
int secondsMask(void);


int hourMask(void);


int getHourFromLittleEndian(int littleEndianNumber);

int getDayFromLittleEndian(int littleEndianNumber);

int getMonthFromLittleEndian(int littleEndianNumber);

int getYearFromLittleEndian(int littleEndianNumber);

int getMinutesFromLittleEndian(int littleEndianNumber);

int getSecondsFromLittleEndian(int littleEndianNumber);

int littleEndianToBigEndian(int littleEndianNumber)
