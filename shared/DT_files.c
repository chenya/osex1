#include "DT_files.h"


FILE* openFile(char* pathToFile, char* mode)
{
    FILE* file;

    file = fopen(pathToFile, mode); 
    
    CHECK_OPEN_FILE_EXIT_ON_ERROR(file, OPEN_FILE_FAILED_MESSAGE) ;


    return file;


}

FILE* openBinFileToWrite(char* pathToFile)
{
    FILE *file;

    file=openFile(pathToFile, BINARY_WRITE);

    return file;
}


FILE* openBinFileToRead(char* pathToFile)
{
    FILE *file;

    file=openFile(pathToFile, BINARY_READ);

    return file;
}

int closeFile(FILE* file)
{

    IF_NULL(file) {
        return CLOSE_FILE_GET_NULL_PTR_ERROR;
    }

    return fclose(file);
}


int writeDT_intStructToBinFile(struct DT_int *dt_int, FILE* file_to_write)
{
     
    return writeStructToBinFileHelper(dt_int, sizeof(*dt_int), file_to_write);
}

int writeDT_bitsStructToBinFile(struct DT_bits *dt_bits, FILE* file_to_write)
{
     
    return writeStructToBinFileHelper(dt_bits, sizeof(*dt_bits), file_to_write);
}
/*
 *int writeStructToBinFile(void* veriable_to_write, FILE* file_to_write)
 *{
 *    return writeStructToBinFileHelper(veriable_to_write, dummySize, file_to_write);
 *}
 */

//int writeStructToBinFileHelper(void* veriable_to_write, int (*sizeFunc)(void), FILE* file_to_write)
int writeStructToBinFileHelper(void* veriable_to_write, int size, FILE* file_to_write)
{
       return fwrite(veriable_to_write, size , 1, file_to_write ); 
}

int readDT_intStructFromBinFile(struct DT_int *dt_int, FILE* file_to_read)
{
    return readStructFromBinFileHelper(dt_int, sizeof(*dt_int), file_to_read);
}

int readDT_bitsStructFromBinFile(struct DT_bits *dt_bits, FILE* file_to_read)
{
    return readStructFromBinFileHelper(dt_bits, sizeof(*dt_bits), file_to_read);
}

/*
 *int readStructFromBinFile(void* veriable_to_write, FILE* file_to_write)
 *{
 *    return readStructFromBinFileHelper(veriable_to_write, dummySize, file_to_write);
 *}
 */

int readStructFromBinFileHelper(void* veriable_to_read, int size, FILE* read_from_file)
{
       return fread(veriable_to_read, size, 1, read_from_file ); 
}


void writeDatesToFile(struct DT_int* DT_int_arr, int num_of_dates)
{
    FILE *DT_file=openFile(DT_INT_FILE, BINARY_WRITE);
    int i;
    
    for (i = 0; i < num_of_dates; i++) {
        /* code */
        writeDT_intStructToBinFile(&(DT_int_arr[i]), DT_file);
    }

    closeFile( DT_file);

}

void writeBitsToFile(struct DT_bits* DT_bits_arr, int size)
{
    FILE *DT_file=openFile(DT_BITS_FILE, BINARY_WRITE);
    int i;
    
    for (i = 0; i < size; i++) {
        /* code */
        writeDT_bitsStructToBinFile(&(DT_bits_arr[i]), DT_file);
    }

    closeFile( DT_file);

}


int getDT_intStructArrayFromFile(struct DT_int *dt_int, char*  file_path)
{
    int i;
    FILE *read_file = openBinFileToRead(file_path);

    for (i=0; (i < MAX_DT) && !(feof(read_file)) ; i++) {
        readDT_intStructFromBinFile(&(dt_int[i]), read_file);               
    }

    closeFile(read_file);
    /*
     *  return the number of structs have been read to array
     */
    return i-1;
}

int getDT_bitsStructArrayFromFile(struct DT_bits *dt_bits, char*  file_path)
{
    int i;
    FILE *read_file = openBinFileToRead(file_path);

    for (i=0; (i < MAX_DT) && !(feof(read_file)) ; i++) {
        readDT_bitsStructFromBinFile(&(dt_bits[i]), read_file);               
    }

    closeFile(read_file);
    /*
     *  return the number of structs have been read to array
     */
    return i-1;
}
