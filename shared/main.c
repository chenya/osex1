#include "DT_files.h"
#include "defs.h"

int main()
{

    FILE *f;

    struct DT_int d;
    struct DT_int e1;
    struct DT_int e2;

    d.date=15;
    d.seconds=127;

    f=openFile("test.bin", BINARY_WRITE);
    writeDT_intStructToBinFile(&d,f);
    //writeStructToBinFile(&d, f);
    d.date=255;
    d.seconds=2048;

    writeDT_intStructToBinFile(&d,f);
    //writeDT_intStructToBinFile(&d,f);
    closeFile(f);

    
    f=openFile("test.bin", BINARY_READ);

    readDT_intStructFromBinFile(&e1, f);
    readDT_intStructFromBinFile(&e2, f);

    printf("e1.a ==> %d, e1.b ==> %d\n", e1.seconds, e1.date);
    printf("e2.a ==> %d, e2.b ==> %d\n", e2.seconds, e2.date);
    
    closeFile(f);

    

    return 0;
}
