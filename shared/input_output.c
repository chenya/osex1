#include "input_output.h"

int getNumberOfPairsFromUser(void)
{
    int number_of_pairs;
    printf("Enter Number of pairs numbers: ");
    scanf("%d",&number_of_pairs);
    return number_of_pairs;
}

void printNewLine(void)
{
    printf("\n");
}

void getDateAndSecondsPairFromUser(struct DT_int* dt_int)
{
    //int extra_days=0;
    scanf("%d %d",&(dt_int->date), &(dt_int->seconds));
    // if (dt_int->seconds/SECONDS_IN_DAY) {
    //     extra_days+=dt_int->seconds/SECONDS_IN_DAY;
    //     dt_int->date+=extra_days;
    //     dt_int->seconds-= (SECONDS_IN_DAY * extra_days);
        
    // }
}

void fillDaysInEachMonth(int* days_in_month )
{
    days_in_month[JANUARY]=JANUARY_DAYS;
    days_in_month[FEBRUARY]=FEBRUARY_DAYS;
    days_in_month[MARCH]=MARCH_DAYS;
    days_in_month[APRIL]=APRIL_DAYS;
    days_in_month[MAY]=MAY_DAYS;
    days_in_month[JUNE]=JUNE_DAYS;
    days_in_month[JULY]=JULY_DAYS;
    days_in_month[AUGUST]=AUGUST_DAYS;
    days_in_month[SEPTEMBER]=SEPTEMBER_DAYS;
    days_in_month[OCTOBER]=OCTOBER_DAYS;
    days_in_month[NOVEMBER]=NOVEMBER_DAYS;
    days_in_month[DECEMBER]=DECEMBER_DAYS;
}

int extractMonthAndDays(int date_in_days, int* days_in_month, int* extract_month)
{
    int days_in_current_year;
    int curr_month;
    int days_delta;

    days_in_current_year=date_in_days % DAYS_IN_YEAR ;
    days_delta = days_in_current_year;

    for (curr_month = JANUARY; (curr_month < MONTHS_IN_YEAR) && ((days_delta - days_in_month[curr_month])   >= 0); curr_month++) {
        days_delta -= days_in_month[curr_month];
    }
 
    *extract_month = curr_month+1;
    return (days_delta==0) ? 1 : (days_delta+1);
}

void transformDateInDaysToRealDate(int date_in_days, int* days_in_month, int* day, int* month, int* year)
{
    *year=EXTRACT_YEAR(date_in_days);
    *day=extractMonthAndDays(date_in_days, days_in_month, month);
}

void getDayMonthYear(int date_in_days, int* day, int* month, int* year)
{
    int days_in_month[MONTHS_IN_YEAR];
    fillDaysInEachMonth(days_in_month);
    
    transformDateInDaysToRealDate(date_in_days, days_in_month, day, month, year);
}

void printActualDate(int date_in_days)
{
    int day, month, year;
    getDayMonthYear(date_in_days, &day, &month, &year);

    printf("%d/%d/%d",day,month,year);
}

//void getHourtMinuteSecond(struct DT_int* dt_int , int* hour, int* minute, int* second)
void getHourtMinuteSecond(int seconds, int* hour, int* minute, int* second)
{
    *hour=*minute=*second=0;
//    days=0;
//    if (seconds /SECONDS_IN_DAY) {
//        days+=seconds/SECONDS_IN_DAY;
//        seconds -= days*SECONDS_IN_DAY;
//    }
    if (seconds != 0) {
        *hour+=EXTRACT_HOUR(seconds); 
        *minute+=(seconds - (*hour)*SECONDS_IN_HOUR ) / SECONDS_IN_MINUTE; 
        *second+=(seconds - (*hour)*SECONDS_IN_HOUR ) % SECONDS_IN_MINUTE; 
    }
    return ;
}

void printActualTime(int seconds)
{
    int hour;
    int minute;
    int second;
    
    
    getHourtMinuteSecond(seconds, &hour, &minute, &second);
    //printf("Actual time: %.2d:%.2d:%.2d",hour,minute,second);
    printf("%.2d:%.2d:%.2d",hour,minute,second);
}


void printActualDateAndTime(struct DT_int* dt_int)
//void printActualDateAndTime(int date, int seconds)
{
    printActualDate(dt_int->date); 
    printf(" ");
    printActualTime(dt_int->seconds); 

}

void getPairsOfDateAndTimeFromUser(struct DT_int* DT_int_Arr, int num_of_pairs)
{
    int i;

    for (i = 0; i < num_of_pairs; i++) {
        getDateAndSecondsPairFromUser(&(DT_int_Arr[i]));
    }
}

void printPairsOfDateAndTime(struct DT_int* DT_int_Arr, int num_of_pairs)
{
    int i;
    for (i = 0; i < num_of_pairs; i++) {
        printActualDateAndTime(&(DT_int_Arr[i])) ;
        printNewLine();
    }
    
}



int littleEndianBitsDate(struct DT_int *dt_int)
{
    int little;
    int hour, minute,second;
    int day, month, year; 
    //int more_days;

    getDayMonthYear(dt_int->date, &day, &month, &year);
    getHourtMinuteSecond(dt_int->seconds, &hour, &minute, &second);
    
    
    little = 0;
    little |= hour;
    little <<=6;
    little |= minute;
    little <<=6;
    little |= second;
    little <<=6;
    little |= (year-2000);
    little <<=4;
    little |= month;
    little <<=5;
    little |= day;

    return little;
}


void createDT_bitsArrayFromDT_intArray(struct DT_int *dt_int, struct DT_bits *dt_bits, int size)
{
   int i;
   for (i = 0; i < size; i++) {
       (dt_bits[i]).DT_little=littleEndianBitsDate(&(dt_int[i]));
       (dt_bits[i]).DT_big=LITTLE_ENDIAN_TO_BIG_ENDIAN((dt_bits[i]).DT_little);
   }
}

void printLittleEndianActualDateAndTime(int littleEndian)
{
    printf("%d/%d/%d ", getDayFromLittleEndian(littleEndian), 
                        getMonthFromLittleEndian(littleEndian), 
                        getYearFromLittleEndian(littleEndian));
    printf("%.2d:%.2d:%.2d",
                        getHourFromLittleEndian(littleEndian), 
                        getMinutesFromLittleEndian(littleEndian), 
                        getSecondsFromLittleEndian(littleEndian));
}

void printLittleEndianAndBigEndianInHexa(struct DT_bits *dt_bits)
{
    printf("%x %x", dt_bits->DT_little,dt_bits->DT_big);
}


void printTheDateAndTimesAndHexaRepresantation(struct DT_bits *dt_bits)
{
    printLittleEndianActualDateAndTime(dt_bits->DT_little);
    printf("\t");
    printLittleEndianAndBigEndianInHexa(dt_bits);
}


void printDT_bitsStructArray(struct DT_bits *dt_bits, int size)
{
    int i;

    for (i = 0; i < size; i++) {
        printLittleEndianAndBigEndianInHexa(&(dt_bits[i]));
        printNewLine();
    }
}
void printDT_bits_Ptr_StructArray(struct DT_bits **dt_bits, int size)
{
        int i;

    for (i = 0; i < size; i++) {
        //printLittleEndianAndBigEndianInHexa((dt_bits[i]));
        printTheDateAndTimesAndHexaRepresantation(dt_bits[i]);
        printNewLine();
    }
}
int getHowToSortDT_bits_PtrArrayFromUser(void)
{
    int how_to_sort_the_array;
    printf("How to sort DT_bits_Ptr_Arr array?: ");
    scanf("%d",&how_to_sort_the_array);
    
    return how_to_sort_the_array;
}

