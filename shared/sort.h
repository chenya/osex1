#ifndef SORT_H

#define SORT_H
#include <stdio.h>
#include <stdlib.h>
#include "defs.h"
#include "DT_bits.h"


int compare_by_month(const struct DT_bits*, const struct DT_bits*);
int compare_by_year(const struct DT_bits*, const struct DT_bits*);
int compare_by_second(const struct DT_bits*, const struct DT_bits*);

int compareDT_bitsStructByMonth(const void* dt_bits1, const void* dt_bits2);
int compareDT_bitsStructByYear(const void* dt_bits1, const void* dt_bits2);
int compareDT_bitsStructBySecond(const void* dt_bits1, const void* dt_bits2);

void sortBitsStructPointerArray(struct DT_bits** dt_ptr_bits, int size, int how_to_sort);
#endif /* end of include guard: SORT_H */
